## Installation de visual studio code

### présentation

Visual Studio Code est un éditeur de texte avancé qui permet d'éditer du code et aussi de l'éxécuter via son terminal.

Il a beaucoup d'autres fonctionnalités que l'on ne détaillera pas ici.

### Téléchargement

Cliquer sur https://code.visualstudio.com/download.

Choisir la version pour windows.

### Installation

Une fois le fichier d'installation téléchargé, exécuter ce fichier.

Il n'y a pas de contrainte sur le répertoire d'installation.

La contrainte à respecter est de cocher la case "ADD TO PATH", sinon l'installation devra être refaite.

### Test

Une fois l'installation terminée redémarrer l'ordinateur

Une fois redémarré, appuyer sur la touche windows de votre clavier (en bas à gauche la touche avec 4 carrés)

Taper "power" pour lancer le powershell de windows.

Le powershell de windows permet d'entrer des lignes de commandes sous windows.

Testez votre installation en tapant:

```
code .
```

Si on vous indique que la commande n'est pas connue,  vous n'avez pas dû cocher la case "ADD TO PATH" et il faut désinstaller puis réinstaller vscode, sinon passer à la suite.

