## Installation de git:

Télécharger git bash sur ce lien https://gitforwindows.org/

Choisir le repertoire d'installation facile à retrouver, par exemple "C:\Programmes\Git"

Ne pas oublier de cocher la case "Add to PATH" pendant l'installation, afin de rendre la commande git disponible.

Une fois l'installation terminée, il faut ajouter l'adresse du proxy via la commande à taper dans l'invite de commande:

```
git config --global http.proxy http://192.168.224.254:3128
```

Git est prêt à être utilisé au lycée avec le réseau wifi.

 

## Création du compte:

Se rendre sur le site https://gitlab.com

Cliquer sur le bouton "Register" pour créer un compte. Attention à ne pas utiliser le register via google ou autre. Il faut créer un compte avec une adresse mail valide et un mot de passe dédié.  

Il faut maintenant configurer votre PC pour vous identifier avec git.

Ouvrir un __powershell__ et taper en remplaçant "votre pseudo" par votre identifiant et idem pour "user.email".
```
git config --global user.name "votre pseudo"
git config --global user.email "mon.mail@mail.fr"
```
 Git reconnaitra à présent que les modifications apportées depuis cet ordinateur seront les vôtres.

## Premier projet:

Sur la page https://gitlab.com, cliquer sur le bouton "Sign in".

Se connecter avec les identifiants que vous venez de créer.

Cliquer sur le bouton "New project" en haut à droite.

Choisir le nom du projet que vous voulez, par exemple "premiere_page_web"

Vous allez obtenir un projet vide avec en page d'accueil les premières commandes git à connaître.

Nous allons relier votre projet avec votre ordinateur afin de pouvoir envoyer ou recevoir facilement des modifications faites par vos collaborateurs sur ce projet, ou par vous même depuis un autre ordinateur.

Cliquer sur le bouton "Cloner" en haut à droite. 

Cliquer ensuite sur le bouton __copier__ dans la ligne "https".

Sur votre ordinateur ouvrir un __Powershell__.

Créer le dossier "projets" directement à la racine du disque C. vous devriez donc être dans C:\projets>

taper alors 
```bash
git clone 
```
puis coller le lien que vous avez copié juste avant. Vous devriez obtenir quelquechose du genre:
```
git clone https://gitlab.com/<pseudo>/premiere_page_web.git
```

Votre ordinateur devrait vous demander de vous identifier avec pseudo et mdp utilisés pour se connecter sur gitlab.com.

Une fois identifié, vous devriez avoir un message de confirmation du clonage et, pour cette fois, que vous avez cloné un projet vide, ce qui est normal puisque nous venons de le créer.

Si jamais vous vous trompez dans vos identifiants, il faut ouvrir le gestionaire d'identification dans windows et tapant "identif" dans la barre de recherche du menu "démarrer". Il faut alors modifier les identifiants dans la ligne correspondant à gitlab.com.

Allons dans le projet: 
```
cd premiere_page_web
code .
```

Une fois VScode lancé dans le projet nous allons pouvoir ajouter des fichiers puis les modifier.

A l'aide de l'explorateur windows (ou en ligne de commande, voir en-dessous..) copiez le contenu de votre répertoire __projets__ dans lequel il doit y avoir votre premier projet, et collez-le dans le répertoire premiere_page_web que vous venez de créer.

Pour copier en ligne de commande il faut taper la commande suivante:
```
xcopy C:\html_css\section_1 C:\projets\premiere_page_web /S
```
la commande __xcopy__ permet de copier le contenu d'un répertoire depuis une source vers une destination. Le commutateur "/S" permet de copier aussi les sous répertoires. Si vous êtes déjà dans le répertoire de destination il suffit de taper:

```
xcopy C:\html_css\section_1 . /S
```

le point signifiant que l'on utilise le répertoire dans lequel on se trouve.


Ouvrir un terminal dans VScode en cliquant sur Terminal puis "new terminal".

tapper alors
```
git status
```
pour avoir une idée de ce qui a été modifié sur votre PC

les fichiers et dossiers ajoutés en rouge.

Pour pouvoir les envoyer en ligne il faut déjà signifier que l'on veut bien les ajouter au projet:
```
git add <le nom du fichier à ajouter>
```
puis taper à nouveau 

```
git status
```
le fichier ajouté est passé en vert.

Pour tout ajouter d'un coup
```
git add --all
```
puis à nouveau 
```
git status
```
normalement tout est passé en vert.
Il faut ensuite préparer l'envoi des fichier en ligne à l'aide de la commande
```
git commit -am "mon premier commit"
```
Le commit réclame un message qui explique très rapidement ce que vous avez modifié dans votre projet.  
Et enfin poussez les modifications en ligne:
```
git push
```
Chercher dans gitlab.com les modifications apportées.

## A faire lorsque l'on travaille sur un projet git
il faut toujours commencer par taper 
```
git pull
```
Pour récupérer les modifications faites par d'autres ou par vous-même depuis un autre PC.
Lorsque vous avez fini de travailler et que vous vous apprétez à pousser votre travail en ligne,
refaire

```
git pull
```
pour s'assurer qu'aucune modification n'a été apportée pendant votre travail.
```
git add <fichiers> ou --all
git commit -am "message de description"
git push
```

## Partager son projet

Sur gitlab.com, selectionner votre projet à partager.
Sur la gauche aller dans settings, puis members.
Entrer l'identifiant de la personne à ajouter, ainsi que les permissions que vous lui accorder.
Je vous conseille de mettre Maintainer pour permettre à cet utilisateur d'avoir tous les droits.
La personne à qui vous avez partagé votre projet pourra alors accéder à votre projet et le modifier.

Vous pouvez alors partager votre projet avec votre binôme et votre enseignant.

## Décrire son projet

Pour décrire votre projet vous devez créer un fichier README.md dans le répertoire dans lequel vous voulez donner une description. Il convient d'en mettre au moins un à la racine du projet.
Dans le terminal de VScode, taper 
```
cd > README.md
```
Vous pouvez y décrire votre projet. Son contenu apparaitra en ligne après la commande 
```
git push
```


## déployer votre page web en ligne
gitlab.com permet de déployer en ligne votre page web, si celle-ci ne contient du html, css, javascript.
Pour cela il suffit de créer un fichier à la racine du projet
```
cd > .gitlab-ci.yml
```
dans lequel vous allez copier le contenu:
```yml
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master
```

il faut biensur pousser ces modifications.
et ensuite sur gitlab.com, régler les options pour rendre la page visible à tous.
il faut aller dans les "settings" de votre projet, puis "général", puis "visibility", et régler "Pages" sur "everyone" et enfin "Save changes".
Le première fois cela peut prendre une trentaine de minutes.
Vous retrouverez l'url de votre page dans "settings", puis "Pages".

Par défaut le serveur donnera la page index.html, il convient donc d'avoir une page qui porte ce nom, sinon il faudra ajouter le nom de votre page d'accueil au bout de l'url donnée.



