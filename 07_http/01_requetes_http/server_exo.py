from flask import Flask, jsonify, request
import json

app = Flask(__name__)

utilisateurs = [
    {
        'nom': 'Pierre',
        'password': 'azerty'
    },
    {
        'nom': 'Paul',
        'password': '12345678'
    },
    {
        'nom': 'Jacques',
        'password': 'Jacques'
    }
]

def noms_utilisateurs():
    return [utilisateur['nom'] for utilisateur in utilisateurs]

def passwords_utilisateurs():
    return [utilisateur['password'] for utilisateur in utilisateurs]

def password_utilisateur(nom):
    res = "utilisateur inconnu"
    for utilisateur in utilisateurs:
        if nom == utilisateur['nom']:
            res = utilisateur['password']
            break  #interompt la boucle, cela n'est pas obligatoire
    return res

@app.route('/index.html', methods = ['GET'])
def index():
    return jsonify('Bienvenue')


@app.route('/users', methods = ['GET'])
def users():
    return jsonify(utilisateurs)

@app.route('/change/pass/<user>/<password>', methods = ['GET'])
def change_pass(user:str,password:str):
    if user in noms_utilisateurs():
        for utilisateur in utilisateurs:
            if user == utilisateur['nom']:
                utilisateur['password'] = password
                break
        res = 'mot de passe modifié avec succès!!'
    else:
        res = 'utilisateur inconnu'
    return jsonify(res)

@app.route('/change/pass-post', methods = ['POST'])
def change_pass_post():
    user_new_pass = json.loads(request.data.decode('utf8'))
    print(user_new_pass)
    if user_new_pass['nom'] in noms_utilisateurs():
        for utilisateur in utilisateurs:
            if user_new_pass['nom'] == utilisateur['nom']:
                utilisateur['password'] = user_new_pass['new_password']
                break
        res = 'mot de passe modifié avec succès!!'
    else:
        res = 'utilisateur inconnu'
    return jsonify(res)
    







        

    



app.run(host='0.0.0.0', port='5001', debug=True)