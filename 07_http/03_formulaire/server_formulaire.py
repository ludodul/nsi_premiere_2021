
from flask import Flask, request, render_template


app = Flask(__name__)


@app.route("/index")
def index():
    return render_template('index.html')

@app.route('/traitement_formulaire/', methods=['POST'])
def traitement_formulaire():
    print(request.form)
    prenom = request.form['firstname']
    print(prenom)
    age = request.form['years']
    classe_2 = request.form['class_number']
    return render_template('/formulaire_traite.html', prenom=prenom, age=age, classe=classe_2)



app.run(host='0.0.0.0', port='5001', debug=True)